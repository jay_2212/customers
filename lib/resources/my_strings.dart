class MyStrings {
  //loading messages
  static const String please_wait = "Please wait...";
  // alert box
  static const String no_internet = "Network is not available";
  // password validation message.
  static const String password_required = "Enter Password";

  static const String appName = "Customers";
  static const String login_screen_title = 'Login';
  static const String invite_screen_title = 'InviteScreen';
  static const String Registration_screen_title = 'Registration';
  static const String ForgetPassword_screen_title = 'Reset';
  static const String firstname_hint = "Enter firstname";
  static const String lastname_hint = "Enter lastname";
  static const String email_hint = "Enter email";
  static const String password_hint = "Enter Password";
  static const String confirmpassword_hint = "Confirm Password";
  static const String ENTER_VALID_EMAIL="Please enter valid email";
}
