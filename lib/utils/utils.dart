import 'dart:async';
import 'dart:io';

import 'package:customers/resources/my_strings.dart';
import 'package:flutter/material.dart';

Future<bool> checkInternetConnection() async {
  bool returnType = false;
  try {
    //If we are connected to wifi, but not logged in to any network security like cyberome,sophos, then we need to put time out, otherwise this method keeps on executing without any result
    final result = await InternetAddress.lookup('google.com')
        .timeout(new Duration(seconds: 15));
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      returnType = true;
    }
  } on SocketException catch (_) {
    returnType = false;
  }
  //time out indicates no internet access
  on TimeoutException catch (_) {
    returnType = false;
  }
  
  return returnType;
}

Widget loadingBar([double? size]) {
  size ??= 50.0;
  return Align(
    alignment: Alignment.center,
  );
}

void navigateToOtherScreen(BuildContext context, Widget screen) {
  //if we use pushReplacement, when user clicks back he will not see previous screen
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => screen));
}

//info alert,with ok button
void showInfoAlert(BuildContext context, String message) {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        key: _keyLoader,
        title: new Text(MyStrings.appName),
        content: new Text(message),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            color: Colors.blue,
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: new Text("OK"),
          ),
        ],
      );
    },
  );
}
//check if internet is available or not
Future<bool> checkIfNetworkIsAvailable() async
{
  bool isNetworkAvailable = await checkInternetConnection();
  return isNetworkAvailable;
}
