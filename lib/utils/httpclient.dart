import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:customers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import "package:http/http.dart" as http;

class HttpClient {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  //default is true
  //true->it will show info alert if API gives error like wrong username,password,etc
  //false->it will not show any alert to user even if their are erros
  bool shouldShowError = false;

  //default is true
  //true->it will show loading view until API response comes
  //false->it will show loading view
  bool shouldShowLoading = false;

  HttpClient({bool shouldShowError = true, bool shouldShowLoading = true}) {
    this.shouldShowError = shouldShowError;
    this.shouldShowLoading = shouldShowLoading;
  }

  Future<http.Response?> postRequest(
      BuildContext context, String apiUrl, Map data) async {
    showLoading(context);
    bool isNetworkAvailable = await checkInternetConnection();
    if (isNetworkAvailable) {
      Map<String, String> headers = new Map();
      var body;
      print("data ::: $data");
      if(apiUrl == "https://e3-qkmountain.qkinnovations.com/element3-backend/api/course/listCustomerCourseNew"){
        final storage = FlutterSecureStorage();
        String? customerToken = await storage.read(key: "token");
        print("custometToken ::: $customerToken");
        headers["Content-Type"] = "application/x-www-form-urlencoded";
        headers["Accept"] = "application/json";
        headers["Authorization"] = "Bearer ${customerToken}";
        body = data;
      }
      else{
        body = json.encode(data);
        headers["Content-Type"] = "application/json";
        headers["Authorization"] = "Basic";
      }
      print("apiUrl :: $apiUrl");
      print("body ::: $body");
      http.Response response =
      await http.post(Uri.parse(apiUrl), headers: headers, body: body);

      response = handleResponse(context, response)!;

      return response;
    } else {
//      handleNoInternet(context);
      //to indicate caller don't need to handle the API response
      return null;
    }
  }

  Future<http.Response?> getRequest(
      BuildContext context, String apiUrl, Map<String, String> data) async {
    showLoading(context);
    print(apiUrl);

    bool isNetworkAvailable = await checkInternetConnection();
    if (isNetworkAvailable) {
      Map<String, String> headers = new Map();

      String params = "";

      if (data != null) {
        apiUrl += "?";
        data.forEach((k, v) {
          params = params + k + "=" + v + "&";
        });
        if (params.length > 0) {
          params = params.substring(0, params.length - 1);
        }
      }

      apiUrl += params;

      headers["Content-Type"] = "application/json";
      headers["Authorization"] = "Basic";

      http.Response response = await http.get(Uri.parse(apiUrl), headers: headers);

      response = handleResponse(context, response)!;

      return response;
    } else {
      handleNoInternet(context);
      //to indicate caller don't need to handle the API response
      return null;
    }

    //String apiUrl = baseUrl + "${url}";
  }


//handle POST and GET response
  //null will be return in case of error,so caller will not process the response, it will be handled here it self by showing alert or logging out
  http.Response? handleResponse(BuildContext context, http.Response response) {
    print("Response : ${response.body}");
    //we received API response
    dismissLoading(context);
    //handle UserID expire and logout the user (400 and 406 indicates expired UserIDs)
    if (response.statusCode == 400 || response.statusCode == 406) {
      return response;
    }
    //success
    if (response.statusCode == 200) {
      return response;
    } else {
      if (response.statusCode == 201) {
        return response;
      }
      //show error only if't is set to true
      if (shouldShowError) {
        var data = json.decode(response.body);
        String message = data['message'];
        showInfoAlert(context, message);
      }
      //to indicate caller doesn't need to handle API response
      print("Null pointer");
    }
  }

  //show loading view
  void showLoading(BuildContext context) {
    if (shouldShowLoading) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            key: _keyLoader,
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                    padding: EdgeInsets.all(16.0),
                    child: new CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.blue),
                    )),
                new Text('Please wait'),
              ],
            ),
          );
        },
      );
    }
    //show loading view only if it is set to true
  }

  //close loading view
  void dismissLoading(BuildContext context) {
    if (shouldShowLoading) {
      Navigator.of(context, rootNavigator: true).pop();
    }
  }

  //internet is not available
  void handleNoInternet(BuildContext context) {
    //we show loading before checking the internet connection, so close this if we don't have internet
    dismissLoading(context);
    if (shouldShowError) {
      print("internet is not available");
      showInfoAlert(context, "No Internet");
    }
  }
}
