import 'dart:ui';

class MyColors {

  static final textColor = Color(0xff414141);
  static final textColorDark = Color(0xff191919);
  static final textColor37 = Color(0x60414141);
  static final darkBlueAccent = Color(0xff023473);
}