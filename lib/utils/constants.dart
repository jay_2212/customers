class Constants {
  static final nameValidator = RegExp(r"^[a-zA-Z]+[a-zA-Z-\' ]*[a-zA-Z]$");
  static final middleNameValidator = RegExp(r"^[a-zA-Z-\' ]+$");
  static final emailValidator =
      RegExp(r"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,})$");
  static final passwordValidator =
      RegExp(r"^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9@$!%*?&]{8,15})$");
}
