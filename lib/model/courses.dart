class Product {
  final List<Courses> feature_course;

  Product({required this.feature_course});

  factory Product.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['feature_course'] as List;
    print(list.runtimeType);
    List<Courses> imagesList = list.map((i) => Courses.fromJson(i)).toList();

    return Product(feature_course: imagesList);
  }
}

class Courses {
  final course_banner;
  final name;

  Courses({required this.course_banner, required this.name});

  factory Courses.fromJson(Map<String, dynamic> parsedJson) {
    return Courses(
        course_banner: parsedJson['course_banner'], name: parsedJson['name']);
  }
}
