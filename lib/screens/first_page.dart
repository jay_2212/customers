import 'package:customers/screens/login.dart';
import 'package:customers/screens/registration.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class FirstPage extends StatefulWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  _FirstState createState() {
    return _FirstState();
  }
}

class _FirstState extends State<FirstPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        initialIndex: 1,
        length: 2,
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(180.0), // here the desired height
            child: AppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: Image.asset(
                "assets/images/header.png",
                fit: BoxFit.fill,
                height: MediaQuery.of(context).size.height,
              ),
              bottom: PreferredSize(
                preferredSize:
                    Size.fromHeight(MediaQuery.of(context).size.height / 6),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      TabBar(
                        tabs: [
                          Tab(
                            text: "Login",
                          ),
                          Tab(
                            text: "SignUp",
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          body: TabBarView(
            children: [LoginPage(), RegistrationPage()],
          ),
        ));
  }
}
