import 'dart:convert';
import 'dart:ui';


import 'package:customers/components/text-fields.dart';
import 'package:customers/screens/login.dart';
import 'package:customers/utils/LmdColors.dart';
import 'package:customers/utils/httpclient.dart';
import 'package:customers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class RegistrationPage extends StatefulWidget {
  RegistrationPage({Key? key}) : super(key: key);

  @override
  _RegistrationPageState createState() => new _RegistrationPageState();
}

var passKey = GlobalKey<FormFieldState>();

class _RegistrationPageState extends State<RegistrationPage> {
  GlobalKey<FormState> _formKey = new GlobalKey();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _confirmpasswordController =
  TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: ListView(
        children: [
          Container(
            margin: EdgeInsets.fromLTRB(16, 30, 24, 16),
            child: Column(
              children: [
                TextFormField(
                  controller: _firstNameController,
                  keyboardType: TextInputType.text,
                  decoration:
                  buildInputDecoration(Icons.person, "Full Name"),
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'Please Enter Name';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 15),
                TextFormField(
                  controller: _lastNameController,
                  keyboardType: TextInputType.text,
                  decoration:
                  buildInputDecoration(Icons.person, "Last Name"),
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'Please enter last name ';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 15),
                TextFormField(
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration:
                  buildInputDecoration(Icons.email, "Email"),
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'Please a Enter';
                    }
                    if (!RegExp(
                        "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                        .hasMatch(value)) {
                      return 'Please a valid Email';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 15),
                TextFormField(
                  controller: _passwordController,
                  obscureText: true,
                  keyboardType: TextInputType.text,
                  decoration:
                  buildInputDecoration(Icons.lock, "Password"),
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'Please a Enter Password';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 15),
                TextFormField(
                  controller: _confirmpasswordController,
                  obscureText: true,
                  keyboardType: TextInputType.text,
                  decoration: buildInputDecoration(
                      Icons.lock, "Confirm Password"),
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'Please re-enter password';
                    }
                    if (_passwordController.text.trim() !=
                        _confirmpasswordController.text.trim()) {
                      return "Password does not match";
                    }
                    return null;
                  },
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(24, 8, 24, 8),
            child: RaisedButton(
              shape: OutlineInputBorder(
                borderSide: BorderSide(
                    color: MyColors.darkBlueAccent, width: 2),
                borderRadius: BorderRadius.circular(26),
              ),
              padding: EdgeInsets.all(14),
              color: MyColors.darkBlueAccent,
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  callRegisterAPI();
                }
              },
              child: Text(
                'Sign Up',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'ProximaNovaBold',
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future callRegisterAPI() async {
    Map<String, String> data = new Map();
    data["first_name"] = _firstNameController.text.trim();
    data["last_name"] = _lastNameController.text.trim();
    data["email"] = _emailController.text.trim();
    data["password"] = _passwordController.text.trim();
    data["password_confirmation"] = _confirmpasswordController.text.trim();
    data['device_token'] = "1";

    try {
      HttpClient httpClient = new HttpClient();
      var syncUserResponse = await httpClient.postRequest(
          context,
          "https://e3-qkmountain.qkinnovations.com/element3-backend/api/user/appRegistration",
          data);
      processRegisterResponse(syncUserResponse!);
    } on Exception catch (e) {
      if (e is Exception) {
        print(e);
      }
    }
  }

  void processRegisterResponse(Response res) {
    if (res != null) {
      var data = json.decode(res.body);
      if (data["message"].toString().contains("User successfully created")) {
        navigateToOtherScreen(context, LoginPage());
      } else {
        showInfoAlert(context, data["error"]);
      }
    }
  }
}
