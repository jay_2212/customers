import 'dart:convert';
import 'package:customers/components/text-fields.dart';
import 'package:customers/screens/home.dart';
import 'package:customers/utils/LmdColors.dart';
import 'package:customers/utils/httpclient.dart';
import 'package:customers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';

class LoginPage extends StatefulWidget {
  LoginPage({String? title});

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> _key = new GlobalKey();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _formKey1 = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey1,
      child: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(16, 30, 24, 16),
            child: Column(
              children: [
                TextFormField(
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration:
                  buildInputDecoration(Icons.email, "Email"),
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'Please a Enter';
                    }
                    if (!RegExp(
                        "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                        .hasMatch(value)) {
                      return 'Please a valid Email';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 15),
                TextFormField(
                  controller: _passwordController,
                  keyboardType: TextInputType.text,
                  decoration:
                  buildInputDecoration(Icons.lock, "Password"),
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'Please a Enter Password';
                    }
                    return null;
                  },
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(24, 8, 24, 8),
            child: RaisedButton(
              shape: OutlineInputBorder(
                borderSide: BorderSide(
                    color: MyColors.darkBlueAccent, width: 2),
                borderRadius: BorderRadius.circular(26),
              ),
              padding: EdgeInsets.all(14),
              color: MyColors.darkBlueAccent,
              onPressed: () {
                if (_formKey1.currentState!.validate()) {
                  callLoginAPI();
                }
              },
              child: Text(
                'Sign In',
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'ProximaNovaBold',
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future callLoginAPI() async {
    Map<String, dynamic> data = new Map();

    data["email"] = _emailController.text.trim();
    data["password"] = _passwordController.text.trim();
    data["device_token"] = "1";

    print("data ::: $data");

    if (checkIfNetworkIsAvailable() != null) {
      print("internet is available");
    } else {
      showInfoAlert(context, "Network is not available");
    }

    try {
      HttpClient httpClient = new HttpClient();
      var syncUserResponse = await httpClient.postRequest(
          context,
          'https://e3-qkmountain.qkinnovations.com/element3-backend/api/user/appLogin',
          data);
      processLoginResponse(syncUserResponse!);
    } on Exception catch (e) {
      if (e is Exception) {
        print(e);
      }
    }
  }

  void processLoginResponse(Response res) async {
    if (res != null) {
      var user = json.decode(res.body);
      if (user['message'] == "success") {
        print('users::: ${user['data']['token']}');
        var customerToken = user['data']['token'];
        final token = FlutterSecureStorage();
        token.write(key: "token", value: customerToken);
        navigateToOtherScreen(context, HomePage());
      } else {
        showInfoAlert(context, user["error"]);
      }
    }
  }
}
