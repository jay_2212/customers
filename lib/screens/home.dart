import 'dart:convert';
import 'dart:io';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:customers/model/courses.dart';
import 'package:customers/screens/login.dart';
import 'package:customers/utils/httpclient.dart';
import 'package:customers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:page_indicator/page_indicator.dart';

class HomePage extends StatefulWidget {
  HomePage({String? title});

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedIndex = 0;
  int _index = 0;
  List images = [];
  List imageName = [];
  List appBarImages = [
    "https://element3-crm-test.s3.eu-central-1.amazonaws.com/courses/1%20H%20Vormittag_1524489489.png",
    "https://element3-crm-test.s3.eu-central-1.amazonaws.com/courses/5%20H%20Privatunterricht_1482961478.png"
  ];
  final PageController controller = new PageController();
  GlobalKey _bottomNavigationKey = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero, () {
      this.callCourseListAPI();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(250.0), // here the desired height
        child: AppBar(
          actions: [
            Row(
              children: [
                Container(
                  width: 40.00,
                  child: Icon(Icons.notification_important_outlined),
                ),
                Container(
                  width: 40.00,
                  child: Icon(Icons.chat_bubble_outline_sharp),
                ),
                Container(
                  width: 40.00,
                  child: Icon(Icons.subscriptions_outlined),
                )
              ],
            )
          ],
          automaticallyImplyLeading: false,
          flexibleSpace: PageIndicatorContainer(
            length: 3,
            child: PageView(
              children: <Widget>[
                Image.asset(
                  "assets/images/header.png",
                  fit: BoxFit.fill,
                ),
                Image.asset(
                  "assets/images/register_bg.png",
                  fit: BoxFit.fill,
                ),
                Image.asset(
                  "assets/images/register_header.png",
                  fit: BoxFit.fill,
                ),
              ],
              controller: controller,
            ),
          ),
          bottom: PreferredSize(
            preferredSize:
                Size.fromHeight(MediaQuery.of(context).size.height / 10),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 100,
            ),
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(bottom: 20),
        child: ListView(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.only(
                  left: 10,
                  top: 20,
                  bottom: 20,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "My Courses",
                      style: TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0),
                    ),
                    SizedBox(height: 2.0,),
                    Text(
                      "See what's hot this winter",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.normal,
                          fontSize: 10.0),
                    ),
                  ],
                )
              )
            ],
          ),
          _buildHorizontalList(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                margin: EdgeInsets.only(
                  left: 10,
                  top: 20,
                  bottom: 20,
                ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Featured Courses",
                        style: TextStyle(
                            color: Colors.blueAccent,
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0),
                      ),
                      SizedBox(height: 2.0,),
                      Text(
                        "Got board? Try something new!",
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 10.0),
                      ),
                    ],
                  )
              )
            ],
          ),
          _buildHorizontalList(),
        ]),
      ),
      bottomNavigationBar: CurvedNavigationBar(
        key: _bottomNavigationKey,
        index: 2,
        height: 60.0,
        items: <Widget>[
          Icon(Icons.home_outlined, size: 30),
          Icon(Icons.book, size: 30),
          Icon(Icons.qr_code_scanner, size: 30),
          Icon(Icons.person, size: 30),
          Icon(Icons.list, size: 30),
        ],
        color: Colors.lightBlueAccent,
        buttonBackgroundColor: Colors.lightBlueAccent,
        backgroundColor: Colors.white30,
        animationCurve: Curves.easeInOut,
        animationDuration: Duration(milliseconds: 400),
        onTap: (index) {
          setState(() {
            selectedIndex = index;
          });
        },
      ),
    );
  }

  Widget _buildHorizontalList() => Container(
        height: 170,
        width: 160,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 7,
            itemBuilder: (context, index) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10, right: 10),
                        width: 160,
                        height: 170,
                        child: images.length == 0 || imageName.length == 0
                            ? Image.asset("assets/images/register_header.png")
                            : Image.network(
                                images[index],
                                fit: BoxFit.fill,
                              ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        alignment: Alignment.bottomLeft,
                        child: images.length == 0 || imageName.length == 0
                            ? Text("Loading ...")
                            : Text(
                                'Course Name\n${imageName[index + 1]}',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12.0),
                              ),
                      ),
                    ],
                  )
                ],
              );
            }),
      );

  Future callCourseListAPI() async {
    print("callCourseListAPI :::::: ");
    Map<String, dynamic> data = new Map();

    data["page"] = "1";
    data["perPage"] = "10";

    if (checkIfNetworkIsAvailable() != null) {
      print("internet is available");
    } else {
      showInfoAlert(context, "Network is not available");
    }

    try {
      HttpClient httpClient = new HttpClient(shouldShowLoading: true);
      var syncUserResponse = await httpClient.postRequest(
          context,
          'https://e3-qkmountain.qkinnovations.com/element3-backend/api/course/listCustomerCourseNew',
          data);
      processLoginResponse(syncUserResponse!);
    } on Exception catch (e) {
      if (e is Exception) {
        print(e);
      }
    }
  }

  void processLoginResponse(Response res) {
    if (res != null) {
      var user = json.decode(res.body);
      print("user:::: ${user['data']}");
      Product product = new Product.fromJson(user['data']);
      print("json :::: ${product.feature_course.length}");

      for (int i = 1; i < product.feature_course.length; i++) {
        images.add(product.feature_course[i].course_banner);
        imageName.add(product.feature_course[i].name);
        setState(() {});
      }
    }
  }
}
